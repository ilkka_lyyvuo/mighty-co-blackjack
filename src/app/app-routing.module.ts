import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { IntroComponent } from './layouts/intro/intro.component';
import { GameComponent } from './layouts/game/game.component';

const routes: Routes = [
  {
    path: '',
    component: IntroComponent
  },
  {
    path: 'game',
    component: GameComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
