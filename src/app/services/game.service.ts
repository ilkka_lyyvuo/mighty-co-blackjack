import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Card } from '../models/card';
import { GameState } from '../models/gamestate';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private readonly cardDeck = new BehaviorSubject<Card[]>([]);
  private readonly playerActiveHand = new BehaviorSubject<Card[]>([]);
  private readonly dealerActiveHand = new BehaviorSubject<Card[]>([]);

  private readonly gameState = new BehaviorSubject<GameState>({
    player: {
      sum: 0,
      valid: true
    },
    dealer: {
      sum: 0,
      valid: true
    },
    started: false,
    ongoing: false,
    playersTurn: true
  });

  constructor() {
    this.initCardDeck();
  }

  initCardDeck(): void {
    const suits: Array<string> = ['heart', 'tile', 'clover', 'pike'];

    for (const suit of suits) {
      let index = 1;
      while (index < 14) {
        this.addCard({
          id: suit + index,
          suit,
          value: index,
          isPlayed: false
        });
        index++;
      }
    }
  }

  get cards(): Card[] {
    return this.cardDeck.getValue();
  }

  set cards(card: Card[]) {
    this.cardDeck.next(card);
  }

  get playerHandCards(): Card[] {
    return this.playerActiveHand.getValue();
  }

  set playerHandCards(card: Card[]) {
    this.playerActiveHand.next(card);
  }

  get dealerHandCards(): Card[] {
    return this.dealerActiveHand.getValue();
  }

  set dealerHandCards(card: Card[]) {
    this.dealerActiveHand.next(card);
  }

  getState(): Observable<any> {
    return this.gameState.asObservable();
  }

  public giveCard(): Card {
    let card: Card;
    let cardFound = false;

    while (!cardFound) {
      const randIndex = Math.floor(Math.random() * this.cards.length);
      card = this.cards[randIndex];

      if (!card.isPlayed) {
        cardFound = true;
      }
    }

    this.addHandCard(card);
    this.setPlayed(card);
    this.setState(card);

    return card;
  }

  private addCard(card: Card) {
    this.cards = [
      ...this.cards,
      card
    ];
  }

  private addHandCard(card: Card) {
    if (this.gameState.getValue().playersTurn) {
      this.playerHandCards = [
        ...this.playerHandCards,
        card
      ];
    } else {
      console.log('annetaan dealerille kortti', card);
      this.dealerHandCards = [
        ...this.dealerHandCards,
        card
      ];
    }

  }

  startGame(): void {
    const status = this.gameState.getValue();
    status.started = true;
    status.ongoing = true;

    this.giveCard();
  }

  restartGame(): void {
    this.gameState.next({
      player: {
        sum: 0,
        valid: true
      },
      dealer: {
        sum: 0,
        valid: true
      },
      started: true,
      ongoing: false,
      playersTurn: true
    });

    this.playerActiveHand.next([]);
    this.dealerActiveHand.next([]);

    this.startGame();
  }

  setPlayed(card: Card): void {
    const index = this.cards.indexOf(card);

    if (index !== -1) {
      this.cards[index] = {
        ...card,
        isPlayed: true
      };

      this.cards = [...this.cards];
    }
  }

  setState(card?: Card) {
    const status = this.gameState.getValue();

    if (status.playersTurn) {
      status.player.sum += card.value;
      status.player.valid = status.player.sum <= 21;

      if (!status.player.valid) {
        status.ongoing = false;
      }
    } else {
      status.dealer.sum += card.value;
      status.dealer.valid = status.dealer.sum <= 21;
    }

    this.gameState.next(status);
  }

  changeTurn(): void {
    const status = this.gameState.getValue();
    status.playersTurn = false;
    this.gameState.next(status);

    this.playDealer();
  }

  playDealer(): void {
    this.giveCard();

    const dealerPlayInterval = setInterval(() => {
      const shouldPlay: boolean = this.gameState.getValue().dealer.valid && this.gameState.getValue().dealer.sum < this.gameState.getValue().player.sum;

      if (shouldPlay) {
        this.giveCard();
      } else {
        clearInterval(dealerPlayInterval);

        const status = this.gameState.getValue();
        status.ongoing = false;
      }
    }, 1000 * 1);
  }
}
