export interface Player {
  sum: number;
  valid: boolean;
}

export interface GameState {
  player: Player;
  dealer: Player;
  started: boolean;
  ongoing: boolean;
  playersTurn: boolean;
}
