export interface Card {
  id: string;
  suit: string;
  value: number;
  isPlayed: boolean;
}
