import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './components/card/card.component';
import { StatsComponent } from './components/stats/stats.component';
import { GameComponent } from './layouts/game/game.component';
import { IntroComponent } from './layouts/intro/intro.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    StatsComponent,
    GameComponent,
    IntroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
