import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game.service';
import { GameState } from '../../models/gamestate';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {
  public gameState: GameState;

  constructor(
    private gameService: GameService
  ) { }

  ngOnInit() {
    this.gameService.getState().subscribe((response: GameState) => this.gameState = response);
  }

  newCard(): void {
    this.gameService.giveCard();
  }

  stay(): void {
    this.gameService.changeTurn();
  }

  start(): void {
    this.gameService.startGame();
  }

  restart(): void {
    this.gameService.restartGame();
  }

}
